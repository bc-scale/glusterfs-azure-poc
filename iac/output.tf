output "bh-ip" {
  description = "Public IP Address of Bastion Host"
  value       = azurerm_linux_virtual_machine.bastion-vm.public_ip_addresses[0]
}
